# **IOT Basics**

IoT is used in several places. When it is used in the Industries, to connect the things to the internet it is called **_industrial IoT_.**

## **Industrial Revolution**

 * Industry **1.0** (_1784_) : After 1784, there were a lot of steam-powered machines introduced to help the workers. This helped in the production of a lot of goods. This was mainly helpful in the textile industry. The increase in mechanization and machines, helped in production a lot as they were faster and easier to operate. This made all kinds of technologies and innovations possible.  

 * Industry **2.0** (_1870_) : Slowly the industries turned to version 2.0. This was also called as the "_The Technological Revolution_". This was because new electrical technology was introduced, making production even easier and a the produced quantities increased.  
  
 * Industry **3.0** (_1969_) : Industry 3.0 started with the usage of computers. Initially the computers were'nt very much powerful. The industries started using automation more. The usage of electrical and IT helped in production. The productions increased even more as many things were automated and introduction of the internet was the main thing that helped in automation and helped in technolgy and innovation.The industry 3.0 uses sensors to get data from the environment and uses PLCs, SCADA and ERP to process the information. The information is stored in as excels or CSV's. They rarely get plotted as graphs. _EtherCat_ , _Modbus_ , _CANopen_ are some protocols used to store the informations in the central server.  

  * Indutry **4.0** (_Currently_) : The Industry 4.0 is the latest version. This includes IIoT, Cloud Computing, Cyber physical systems, Cognitive Computing. The industries have started using smart machines in doing things, making the production easier and efficient. The smart machines use internet to communicate mostly, and many processes have been automated throughout these years making the manufacturing processes easier and efficient.  



![image][revolution]

[revolution]: https://guardian.ng/wp-content/uploads/2018/10/Fourth-Revolution-kk.jpg

> The industry 3.0 has the data stored in databases and it could be viewed as excel.  



#### **The Industry 4.0 is just Industry 3.0 connected to the internet.**
![industry][industrytoday]

[industrytoday]: https://aethon.com/wp-content/uploads/2015/07/Industry40.jpg

* In a typical industry 4.0, the data is collected by the sensors from the real world and is processed. The processed data which is useful to us is uploaded to the cloud using the _IoT gateway_.  
* These use the _MQTT, CoAP, Websockets, AMQP, Restful API_ protocols to communicate and send data to the cloud.Many industries are alligned more towards industry 3.0, and it is difficult to convert to industry 4.0 as this could stop production for some days and also, it will be financially difficult as well.The IIoT protocols are used to send the data from the iot gateway to the cloud.

* The solution to the problem is that getting data using the present industry 3.0 devices and sending the data to the cloud with industry 4.0 devices.   
* Changing the protocols used in industry 3.0 to those used in industry 4.0 as mentioned above can be helpful. The difficulties faced in the conversion are due to the lack of documentation, expensive hardwares etc.  

> **_The Shunya Interfaces library can be used for sending the industry 3.0 devices to the industry 4.0 cloud._**
There are many boards available in the market like the Raspberry PI, that is affordable and cheap that can be used to send the data from the industry 3.0 devices to the industry 4.0 cloud.

## IIoT protocols:
* Some of the protocols used are MQTT,AMQP,CoAP. These protocols are used to send the data from the iotgateway to the cloud.
* The MQTT protocol uses the publisher/subscriber method in communication. It is used in devices that need less power and economical.  
* The CoAP is used for lightweight implementation. The client may send a request to the server, and the server might send the data in HTTP.    
* The AMQP protocol lets the client applications to communicate with the server. The AMQP is used for secure message transfer to the cloud.  
* The IoT data protocols are used for point to point communication of devices.  
* In case of network issues, the protocols like MQTT have the data that has to be sent in queue and send them to the cloud once, the connection is established.  
  

### **The steps involved in making an Industrial IoT product is as follows:**  
1. Identify popular industry 3.0 devices.
2. Find the protocols that these devices use to communicate.
3. Get the data from industry 3.0 devices.
4. Send the data to the cloud for industry 4.0.

* The data is stored in time series databases (TSDB) like Prometheus.The data is seen analysed using Iot platforms like:
  * Amazon AWS
  * Google IoT
  * Azure IoT  
  * Thingsboard


